package cat.itb.recyclerviewexemple

import androidx.lifecycle.ViewModel
import kotlin.random.Random

class SteamViewModel: ViewModel() {
    private var games = mutableListOf<SteamGame>()
    private var genreList =
        arrayOf("Shooter", "RPG", "MOBA",
            "Simulation", "Platforms", "Adventures")
    private var snapshots= arrayOf(R.drawable.portada1,
        R.drawable.portada2,
        R.drawable.portada3)
    init {
        for (i in 1..100) {
            games.add(SteamGame("Game #$i",
                genreList[Random.nextInt(6)],
                "%.2f".format(Random.nextFloat()*150) + "€",
                snapshots[Random.nextInt(3)]))
        }
    }
    //trabajar con esto:
    //wireframepro.mockflow
    //material.io/color tool
    //https://www.fontspace.com/spaceline-font-f55787
    //API definitiva:
    //https://finalspaceapi.com/docs/endpoints/character

    fun getList()=games
}