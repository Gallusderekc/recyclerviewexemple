package cat.itb.recyclerviewexemple

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SteamAdapter(games: List<SteamGame>): RecyclerView.Adapter<SteamAdapter.SteamViewHolder>()  {

    val games = games


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SteamAdapter.SteamViewHolder{
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.recyclerview_item,parent,false)
        return SteamViewHolder(v)
    }

    override fun onBindViewHolder(holder: SteamAdapter.SteamViewHolder, position: Int) {
        holder.bindData(games[position])
    }
    override fun getItemCount():Int = games.size


    class SteamViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView){
        private var  snapImageView: ImageView
        private var  nameTextView:TextView
        private var genreTextView: TextView
        private var priceTextView: TextView
        init{
            snapImageView=itemView.findViewById(R.id.snap_imageview)
            nameTextView=itemView.findViewById(R.id.name_textview)
            genreTextView=itemView.findViewById(R.id.genre_textview)
            priceTextView=itemView.findViewById(R.id.price_textview)
        }

        fun bindData(steamGame: SteamGame){
            snapImageView.setImageResource(steamGame.snapshot)
            nameTextView.text = steamGame.name
            genreTextView.text = steamGame.genre
            priceTextView.text = steamGame.price
        }

    }

}